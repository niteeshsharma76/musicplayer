//
//  HomeViewController.m
//  musIcPlayer
//
//  Created by Click Labs130 on 10/12/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import "HomeViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface HomeViewController ()
{
    AVAudioPlayer *_audioPlayer;
    UITableView *tableView;
}
@end
UIButton *play;
UIButton *pauseBtn;
UIButton *nextBtn;
UIButton *prevBtn;
NSArray *soundsArray;
NSString *path;
NSString *soundFile;
NSURL *sondUrl;
UISlider *volumeControl;
@implementation HomeViewController
int i = 0;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    
     //NSUInteger index = arc4random_uniform(soundsArray.count);
     
     //NSString *path = [soundsArray objectAtIndex:index];
     //NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:path ofType: @"mp3"];
    // NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    
   // NSUInteger index = arc4random_uniform(self.soundsArray.count);

    
   
    self.view.backgroundColor = [UIColor lightGrayColor];
 soundsArray =[[NSArray alloc] initWithObjects:@"2", @"1", @"3", nil];
    //path= [self.soundsArray objectAtIndex:i];
     path = [NSString stringWithFormat:@"%@/%@.mp3", [[NSBundle mainBundle] resourcePath],soundsArray[i]];
     NSLog(@"%@",path);
    //soundFile = [[NSBundle mainBundle] pathForResource:path ofType: @"mp3"];
sondUrl = [[NSURL alloc] initFileURLWithPath: path];
    _audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:sondUrl error:nil];

   
    /*soundFile = [[NSBundle mainBundle] pathForResource:path ofType: @"mp3"];
    sondUrl = [[NSURL alloc] initFileURLWithPath: soundFile];
    
    _audioPlayer= [[AVAudioPlayer alloc]initWithContentsOfURL:sondUrl error:nil]; */
    
    tableView=[[UITableView alloc]init];
    tableView.frame = CGRectMake(10,30,320,400);
    tableView.dataSource=self;
    tableView.delegate=self;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [tableView reloadData];
    [self.view addSubview:tableView];
    
    
    
    play=[[UIButton alloc ]init];
    play.frame=CGRectMake(100, 430, 70, 70);
    UIImage *btnImage = [UIImage imageNamed:@"play.png"];
    [play setImage:btnImage forState:UIControlStateNormal];
    [play addTarget:self action:@selector(playMusic) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:play];
    
    pauseBtn=[[UIButton alloc ]init];
    pauseBtn.frame=CGRectMake(180, 430, 70, 70);
    UIImage *btnImage2 = [UIImage imageNamed:@"pause.png"];
    [pauseBtn setImage:btnImage2 forState:UIControlStateNormal];
    [pauseBtn addTarget:self action:@selector(pauseMusic) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:pauseBtn];

    nextBtn=[[UIButton alloc ]init];
    nextBtn.frame=CGRectMake(270, 430, 70, 70);
    UIImage *btnImage3 = [UIImage imageNamed:@"next.png"];
    [nextBtn setImage:btnImage3 forState:UIControlStateNormal];
    [nextBtn setUserInteractionEnabled:YES];
    
    [nextBtn addTarget:self action:@selector(nextMusic) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:nextBtn];
    
    prevBtn=[[UIButton alloc ]init];
    prevBtn.frame=CGRectMake(20, 430, 70, 70);
    //prevBtn.layer.cornerRadius=2;
    UIImage *btnImage4 = [UIImage imageNamed:@"prev.jpg"];
    [prevBtn setImage:btnImage4 forState:UIControlStateNormal];
    
    [prevBtn addTarget:self action:@selector(prevMusic) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:prevBtn];
    
    volumeControl=[[UISlider alloc]init];
    volumeControl.frame=CGRectMake(10.0, 520.0, 180.0, 5.0);
    [volumeControl addTarget:self action:@selector(sliderAction) forControlEvents:UIControlEventValueChanged];
    [volumeControl setBackgroundColor:[UIColor whiteColor]];
   volumeControl.minimumValue = 0.0;
   volumeControl.maximumValue = 90.0;
 volumeControl.continuous = YES;
    //volumeControl.userInteractionEnabled=YES;
    volumeControl.value = 20.0;
    [self.view addSubview:volumeControl];
    
    
    }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier   forIndexPath:indexPath] ;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text=[soundsArray objectAtIndex:i]; 
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Your custom operation
}

-(void) sliderAction{
  
    _audioPlayer.volume = volumeControl.value ;
}


-(void) playMusic {
    
    
        [_audioPlayer play];
    }


-(void) pauseMusic {
    [_audioPlayer pause];
    
}
-(void) nextMusic
  {
      prevBtn.hidden=NO;
    if(i==2)
{
    nextBtn.hidden=YES;
    NSLog(@"Last Music");
    [_audioPlayer play];

}
else{
     i++;
    [_audioPlayer stop];
   path = [NSString stringWithFormat:@"%@/%@.mp3", [[NSBundle mainBundle] resourcePath],soundsArray[i]];
   sondUrl = [[NSURL alloc] initFileURLWithPath: path];
    _audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:sondUrl error:nil];
    
    [_audioPlayer play];
}
} 
-(void) prevMusic {
     nextBtn.hidden=NO;

    if(i==0)
    {
          prevBtn.hidden=YES;
        NSLog(@"First Music");
        [_audioPlayer play];

    }
    else{
        i--;
    [_audioPlayer stop];
    path = [NSString stringWithFormat:@"%@/%@.mp3", [[NSBundle mainBundle] resourcePath],soundsArray[i]];
    sondUrl = [[NSURL alloc] initFileURLWithPath: path];
    _audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:sondUrl error:nil];
    
    [_audioPlayer play];
    }
    }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



@end
