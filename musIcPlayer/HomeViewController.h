//
//  HomeViewController.h
//  musIcPlayer
//
//  Created by Click Labs130 on 10/12/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>


@interface HomeViewController : UIViewController
@property (strong, nonatomic) NSArray *soundsArray;


//-(void) initPlayer:(NSString*) audioFile fileExtension:(NSString*)fileExtension;
//- (void)playAudio;
//- (void)pauseAudio;
//- (void)setCurrentAudioTime:(float)value;
//- (float)getAudioDuration;
//- (NSString*)timeFormat:(float)value;
//- (NSTimeInterval)getCurrentAudioTime;

@end
